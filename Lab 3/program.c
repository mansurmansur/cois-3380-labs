/**********************************************************************************************************************/
/*
 *      COIS 3380H : SYSTEM PROGRAMMING
 *      -------------------------------
 *
 *      Author: Mansur Mansur                                         DATE: Feb 28th 2020, Friday
 *      ---------------------                                         ---------------------------
 *
 *      Description:
 *      -----------
 *
 *
 * */
/**********************************************************************************************************************/

#include <stdio.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <dirent.h>
#include <getopt.h>
#include <unistd.h>
#include <stdbool.h>
#include <sys/stat.h>
#include <time.h>

#define  PATH_LENGTH 128;  /*only allow the code to process paths less than 128 characters long*/

/** Global variables*/
char target_dir[PATH_LENGTH];    /*directory path*/
bool flag_b;            /*flag -b*/
bool flag_s;            /*flag -s*/
int  valWithFlag_b;     /*int value that follows flag -b*/
int  valWithFlag_s;     /*int value that follows flag -s*/




/*functions*/
void readDirectoryContent(DIR *dp);
void setStatInfo(char file[35]);
void printFileInfo(struct stat fileStat, char* filename);
char* fileType(struct stat fileStat);
char* dateFormat(char* str, time_t val);


/**********************************************************************************************************************/
/*
 * Name:                readDirectoryContent
 * Description:
 *
 * algorithm:
 *          1.
 *          2.
 * return type:         void
 * local variables:
 * */
/**********************************************************************************************************************/
int main(int argc, char *argv[]) {

    /*variables*/
    DIR *dp;      //directory to be listed
    int arg_length;
    int options;
    extern char *optarg;

    /*check the arguments*/
    /*if no path is given*/
    if(argc == 1){
        strcpy(target_dir,".",1);
    } else{
        arg_length = strlen(argv[1]);

        /*path length should not be more than 120*/
        if(arg_length > PATH_LENGTH){
            printf("directory path not supported");
            exit(1);
        }

        /*while loop that loops*/
        while (optind < argc){
            /*loop the arguments with getopt*/
            if(options = getopt(argc,argv,"-s:-b:") != -1){
                switch (options){
                    case '-s':
                        flag_s = true;
                        valWithFlag_s = atoi(optarg);
                        break;
                    case '-b':
                        flag_b = true;
                        valWithFlag_b = atoi(optarg);
                        break;
                    case '?':
                        printf("You gave an unrecognized parameter: -%c. \n", optopt);
                        break;
                    default:
                        printf("Something went wrong... \n");
                        break;
                }else{
                    /*get directory path from last argument*/
                    target_dir = argv[optind];
                    optind++;  //end loop
                }
            }
        }

    }


    /*header of the table*/
    printf("%-6s%-5s%-5s%-5s%-10s%-35s%-10s\n", "Inode", "Type", "UID", "GID", "ByteSize", "Filename", "Modification Date");

    /*open directory*/
    if( (dp = opendir(target_dir)) == NULL )
    {
        fprintf( stderr, "Cannot open dir\n" );
        return;
    } else{
        /*invoke the read function which reads the content of the method*/
        readDirectoryContent(dp);
    }


    /*close the open directory*/
    closedir(dp);
    return 0;
}

/**********************************************************************************************************************/
/*
 * Name:                readDirectoryContent
 * Description:
 *
 * algorithm:
 *          1.
 *          2.
 * return type:         void
 * local variables:
 * */
/**********************************************************************************************************************/
void readDirectoryContent(DIR *dp)
{
    struct dirent *dir;        /*pointer to struct dirent*/

    /*read entries*/
    while ((dir = readdir(dp)) != NULL){
        /*"." and ".."*/
        if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0) continue;

        /*path for the file to*/
        char file[35];
        strcpy(file, target_dir);
        strcat(file,dir->d_name,"/");
        strcat(file,dir->d_name);


        /*read the file*/
        setStatInfo(file);
    }
}

/**********************************************************************************************************************/
/*
 * Name:                printFileProperties
 * Description:
 *
 * algorithm:
 *          1.
 *          2.
 * return type:
 * local variables:
 * */
/**********************************************************************************************************************/
void printFileInfo(struct stat fileStat, char* filename){
    /*filename or directory name*/
    char *filetype;
    struct stat stats;


    /*stat*/
    stat(path,&stats);

    /*assign*/
    printf("%-6ld",(long)stats.st_ino);
    printf("%-5s",filetype = fileType(fileStat)); //Prints the file's filetype
    printf("%-5ld", (long) stats.st_uid); //print the files owner user id
    printf("%-5ld", (long) stats.st_gid); //print the files owner group id
    printf("%-10lld", (long long) stats.st_size); // print the file size
    printf("%-35s", filename); //Name of the file
    printf("%-10s", ctime(&fileStat.st_mtime)); // last modified time
}


/**********************************************************************************************************************/
/*
 * Name:                printFileProperties
 * Description:
 *
 * algorithm:
 *          1.
 *          2.
 * return type:
 * local variables:
 * */
/**********************************************************************************************************************/
void setStatInfo(char file[35]){
    struct stat file_stats;

    if(stat(file, &file_stats) == 0){
            //If we were given the -b or -s flags. Do checks on the current file to see if we should display it or not.
            if(valWithFlag_b || valWithFlag_s) {

                if(valWithFlag_b) {
                    //Display files from BEFORE beforeDays ago.
                    time_t currentTime = time(0); //Current time
                    double diff = difftime(currentTime, file_status.st_mtime); //Difference between current time and the file's modified time.
                    int days = (diff* 3600/24); //Convert seconds to days
                    if(days < valWithFlag_b) {
                        //DONT SHOW. TOO OLD of a file
                        continue; //Go on to next file. Skip current one.
                    }
                }
                //sinceDays = age of newest files to display SINCE.
                if(valWithFlag_s) {
                    //Display files from AFTER sinceDays ago.
                    time_t currentTime = time(0);
                    //Are the number of seconds greater then number of days to seconds?
                    double diff = difftime(currentTime, file_stats.st_mtime); //Difference between current time and the file's modified time
                    int days = diff * 3600/24; //Convert seconds to days
                    if(days > valWithFlag_s){
                        //DONT SHOW. TOO NEW of a file
                        continue; //Skip this file. Move along to next one
                    }
                }
            }

            //IF the file hasn't been skipped display the information for the file
            FileProperties(file_stats, dp->d_name);
    } else {
            //We couldn't load the file's information. Print an error and move along to the next file
            printf("INVALID FILE PATH: %s \n", file);
    }

}

/**********************************************************************************************************************/
/*
 * Name:                printFileProperties
 * Description:
 *
 * algorithm:
 *          1.
 *          2.
 * return type:
 * local variables:
 * */
/**********************************************************************************************************************/
char* fileType(struct stat fileStat){
    char* filetype;

    switch (fileStat.st_mode & S_IFMT) // parse out the file type
    {
        case S_IFREG:
            filetype = "REG";
            break;
        case S_IFDIR:
            filetype = "DIR";
            break;
        case S_IFCHR:
            filetype = "C_DEV";
            break;
        case S_IFBLK:
            filetype = "B_DEV";
            break;
        case S_IFLNK:
            filetype = "LINK";
            break;
        case S_IFIFO:
            filetype = "FIFO";
            break;
        case S_IFSOCK:
            filetype = "SOCK";
            break;
        default:
            filetype = "UNKN";
            break;
    }

    return filetype;
}



/**********************************************************************************************************************/
/*
 * Name:                printFileProperties
 * Description:
 *
 * algorithm:
 *          1.
 *          2.
 * return type:
 * local variables:
 * */
/**********************************************************************************************************************/
char* formatDate(char* str, time_t val){
    strftime(str, 40, "%d.%m.%Y %H:%M:%S", localtime(&val));
    return str;
}