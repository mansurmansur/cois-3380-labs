# Repository Name
Name: COIS 3380 - Labs


# Descriptions of the COIS 3380
Systems programming produces software that provides sevices to the computer 
hardware. Using systems programming techniques in UNIX-style environment and 
machine-oriented programming language, core topics will include procedural 
programming, shell programming, pipes, file processing, system calls, signals 
and basic network programming. 

procedural programming language used : C
script language used                 : shell script


# Labs
There are six labs which are independently worked on. Each lab has instructions 
and answer to those questions asked.


# Note:
This work was done for the course credit

